import { Component, OnInit } from '@angular/core';
import * as lozad from 'lozad';

@Component({
  selector: 'app-lazy',
  templateUrl: './lazy.component.html',
  styleUrls: ['./lazy.component.scss']
})
export class LazyComponent implements OnInit {

  constructor() { 
    console.log("starting lozad");
  }

  ngOnInit() {
    lozad('.lazy', {
      load: function (el:any) {
        el.src = el.dataset.src;
        el.onload = function () {
          el.classList.add('fade');
        }
      }
    }).observe();
  }

  saludar(){
    alert("Hola");
  }
}
